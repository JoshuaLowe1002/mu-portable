# mu-portable
A portable version of Nicholas Tollervey's Mu Python Editor for you to run Mu from a pendrive.

![Header Image](mu-portable.png)
